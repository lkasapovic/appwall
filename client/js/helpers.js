"use strict";

Template.posts.helpers({
    postList: function () {
        return Collections.Post.find({}, {
            sort: {
                createdAt: -1
            }
        });
    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                gutter: 20,
                columnWidth: '.grid-sizer',
                percentPosition: true
            });
        });
    },

    imageIs: function (ImageID) {
        var image = Collections.Images.find({
            _id: this.ImageID
        }).fetch();
        return image[0].url();
    }
});
